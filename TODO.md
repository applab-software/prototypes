* ~~Create a container for storing Archetypes (e.g. ArchetypeContainer)~~
* ~~Allow pushing components & entities to this container.~~  
  ~~Archetypes should be dynamically created as required.~~
* Add support for adding/removing components from entities.  
  All components from the entity should be moved from one archetype to another.
* The `ArchetypeBuilder` doesn't work for single components.
* `len` is not being set after `ComponentVec::grow` is called  
  Update: This is being done, but it is hacky as hell

---

### Registering components with world

All components need to be registered with the world.  
There will be a `ComponentInfo` struct that contains data such as the `Name`, `TypeId`, `Layout`, and `Drop` function.  

The `ComponentVec` struct `new` function will be changed to take this data directly, rather than  
relying on the use of generics.  
The use of generics here is hindering the creation of Archetypes in the `get_or_insert` call.

This data can also be fetched and referenced as required by other functions.

### Adding component to entity

First the ECS should find the current archetype the entity belongs to.  

Then it should get the archetype to move the components to (or create a new one if required).  
This should be easily done by adding the typeid of the new component to the current archetype hashset, and using  
that to search for a matching archetype.

Next, all component and the entity need to be transferred to the new archetype, and the archetype  
storage grown to accommodate the components.
A simple `ptr::swap` should be enough for this.

At this point, the storage for the new component will be uninitialised.  
The new component should then be added into the new archetype.

**NOTE:** Try adding the components in a plain function.  
Doing things as part of the world causes borrow checker conflicts.  
For reference, check out what Bevy is doing.
